package com.example.reto5.modelos;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table
public class Pelicula {
    @Id
    Long ID_pelicula;
    String titulo;
    String resumen;
    String anio;
    String nombre_director;

    public Long getID_pelicula() {
        return ID_pelicula;
    }

    public void setID_pelicula(Long ID_pelicula) {
        this.ID_pelicula = ID_pelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getNombre_director() {
        return nombre_director;
    }

    public void setNombre_director(String nombre_director) {
        this.nombre_director = nombre_director;
    }
    
}
