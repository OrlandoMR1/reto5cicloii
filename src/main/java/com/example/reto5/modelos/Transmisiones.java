package com.example.reto5.modelos;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Transmisiones { 
    @Id
    Long ID_transmisiones;
    String ID_username;
    String Contenido; 	
    String fecha_hora;

    public Long getID_transmisiones() {
        return ID_transmisiones;
    }

    public void setID_transmisiones(Long ID_transmisiones) {
        this.ID_transmisiones = ID_transmisiones;
    }

    public String getID_username() {
        return ID_username;
    }

    public void setID_username(String ID_username) {
        this.ID_username = ID_username;
    }

    public String getContenido() {
        return Contenido;
    }

    public void setContenido(String Contenido) {
        this.Contenido = Contenido;
    }

    public String getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(String fecha_hora) {
        this.fecha_hora = fecha_hora;
    }
    
}
