package com.example.reto5.modelos;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table
public class Contenido {
    @Id
    Long dir_contenido;
    //@Id
    //@GenerateValue(strategy = GenerationType.IDENTITY)
    String titulo_contenido;
    String tipo_contenido;

    public Long getDir_contenido() {
        return dir_contenido;
    }

    public void setDir_contenido(Long dir_contenido) {
        this.dir_contenido = dir_contenido;
    }

    public String getTitulo_contenido() {
        return titulo_contenido;
    }

    public void setTitulo_contenido(String titulo_contenido) {
        this.titulo_contenido = titulo_contenido;
    }

    public String getTipo_contenido() {
        return tipo_contenido;
    }

    public void setTipo_contenido(String tipo_contenido) {
        this.tipo_contenido = tipo_contenido;
    }
    
}