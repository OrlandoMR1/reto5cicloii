package com.example.reto5.repositorios;

import com.example.reto5.modelos.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author johan
 */
public interface Repositorio_Usuario extends JpaRepository<Usuario, String>{
    
}
