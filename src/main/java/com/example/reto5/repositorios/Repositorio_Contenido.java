package com.example.reto5.repositorios;

import com.example.reto5.modelos.Contenido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author johan
 */
@Repository
public interface Repositorio_Contenido extends JpaRepository<Contenido, Long>{
    
}
