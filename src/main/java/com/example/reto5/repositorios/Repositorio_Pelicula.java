package com.example.reto5.repositorios;

import com.example.reto5.modelos.Pelicula;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author johan
 */
public interface Repositorio_Pelicula extends JpaRepository<Pelicula, Long>{
  
}
