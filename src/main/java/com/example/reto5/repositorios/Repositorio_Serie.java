package com.example.reto5.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import com.example.reto5.modelos.Serie;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author johan
 */
@Repository
public interface Repositorio_Serie extends JpaRepository<Serie, Long>{
    
       //List<Serie> findBySerie_Titulo(String SerieTitulo);
       //Optional<Serie> findOne(String SerieTitulo);
        List<Serie> findByTitulo(String titulo);
}

