package com.example.reto5.repositorios;

import com.example.reto5.modelos.Transmisiones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author johan
 */
public interface Repositorio_Transmisiones extends JpaRepository<Transmisiones, Long>{
    
}
