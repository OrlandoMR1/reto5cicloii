/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reto5.vistas;

import com.example.reto5.Reto5Application;
import com.example.reto5.SpringContext;
import com.example.reto5.modelos.Serie;
import com.example.reto5.repositorios.Repositorio_Serie;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;
import static javafx.scene.paint.Color.rgb;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 *
 * @author johan
 */
public class Ventana extends javax.swing.JFrame {

    Repositorio_Serie repositorio_Serie;
    static int bandera = 0;
    
    public Ventana() {
    
        Reto5Application.iniciarSpring();
        
        repositorio_Serie = SpringContext.getBean(Repositorio_Serie.class);
        initComponents();
        //setResizable(false); // maximize button disable
        
        visualTextos(false,false,false,false);
        ImageIcon img = new ImageIcon("files/r.png");
        setIconImage(img.getImage());
       
        this.getContentPane().setBackground(Color.black);
        
    }

private static int createUI(final JFrame frame){  
            
            int result = JOptionPane.showConfirmDialog(frame,"¿Está seguro que quiere eliminar esta serie?", "Eliminar Serie",
               JOptionPane.YES_NO_OPTION,
               JOptionPane.QUESTION_MESSAGE);
            if(result == JOptionPane.YES_OPTION){
                return 1;
            }else if (result == JOptionPane.NO_OPTION){
               return 0;
            }else {
              return 0;
            }
            
    
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_serie_id = new javax.swing.JTextField();
        txt_serie_titulo = new javax.swing.JTextField();
        txt_serie_temp = new javax.swing.JTextField();
        txt_serie_epi = new javax.swing.JTextField();
        Opcion = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        Cancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("G3M4E13 - CRUD JAVA - Spring JPA");
        setBackground(new java.awt.Color(30, 41, 53));
        setForeground(new java.awt.Color(30, 41, 53));
        setMaximumSize(new java.awt.Dimension(875, 967));
        setName("frame"); // NOI18N
        setResizable(false);
        setSize(new java.awt.Dimension(875, 967));

        txt_serie_id.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_serie_id.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_serie_id.setToolTipText("");
        txt_serie_id.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txt_serie_id.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_serie_idKeyPressed(evt);
            }
        });

        txt_serie_titulo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_serie_titulo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_serie_titulo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        txt_serie_temp.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_serie_temp.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_serie_temp.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txt_serie_temp.setMinimumSize(new java.awt.Dimension(6, 30));
        txt_serie_temp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_serie_tempKeyPressed(evt);
            }
        });

        txt_serie_epi.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txt_serie_epi.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_serie_epi.setToolTipText("");
        txt_serie_epi.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txt_serie_epi.setName(""); // NOI18N
        txt_serie_epi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_serie_epiKeyPressed(evt);
            }
        });

        Opcion.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        Opcion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Elije una opción", "Crear", "Leer", "Actualizar", "Borrar" }));
        Opcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpcionActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jButton1.setText("Aceptar");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarButton(evt);
            }
        });

        Cancelar.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        Cancelar.setText("Cancelar");
        Cancelar.setToolTipText("");
        Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CancelarAceptarButton(evt);
            }
        });

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(153, 153, 153));
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(230, 180, 82));
        jLabel3.setText("Episodios:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(230, 180, 82));
        jLabel4.setText("ID:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(230, 180, 82));
        jLabel5.setText("Titulo:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(230, 180, 82));
        jLabel6.setText("Temporadas:");

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Top.png"))); // NOI18N

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Bottom.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel9))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(836, 836, 836)
                                        .addComponent(jLabel1))
                                    .addComponent(jLabel2)))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel8))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 769, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(97, 97, 97)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_serie_id, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(Opcion, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(txt_serie_temp, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txt_serie_epi, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(txt_serie_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 796, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(247, 247, 247)
                                .addComponent(jButton1)
                                .addGap(83, 83, 83)
                                .addComponent(Cancelar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(436, 436, 436)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(Opcion, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_serie_id, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_serie_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_serie_temp, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_serie_epi, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(Cancelar))
                        .addGap(32, 32, 32)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(13, 13, 13)
                .addComponent(jLabel9))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    
    private void Actualizar() {                                           
        // TODO add your handling code here:

        if( txt_serie_titulo.getText().equals("") || txt_serie_temp.getText().equals("") || txt_serie_epi.getText().equals("")){
        jTextArea1.setText("Ïnformación incompleta para actualizar la serie");
        }
        else {
            
        
                String serie_titulo = txt_serie_titulo.getText();
                Long serie_tempo = Long.parseLong(txt_serie_temp.getText());
                Long serie_epi = Long.parseLong(txt_serie_epi.getText());
        
                String Titulo = txt_serie_titulo.getText();
                List<Serie> s = repositorio_Serie.findByTitulo(Titulo);
        if(s.isEmpty()){
            jTextArea1.setText("No se encontro la serie");
            borrarTextos();
            
        }else{
            s.get(0).setTitulo(serie_titulo);
            s.get(0).setTemp(serie_tempo);
            s.get(0).setEpi(serie_epi);
            repositorio_Serie.save(s.get(0));
            //repositorioreto5.findById(dir_id);
            //repositorioreto5.delete(d);
            System.out.println("Serie actualizada");
            jTextArea1.setText("La Serie " + serie_titulo + " ha sido actualizada");
            borrarTextos();
        }
        
        }
        
        
        
    }                                          

    private void Crear() {                                         
        // TODO add your handling code here:
        if( txt_serie_titulo.getText().equals("") || txt_serie_temp.getText().equals("") || txt_serie_epi.getText().equals("")){
        jTextArea1.setText("Ïnformación incompleta para crear serie");
        }
        else {
            
        String serie_titulo = txt_serie_titulo.getText();
        Long serie_tempo = Long.parseLong(txt_serie_temp.getText());
        Long serie_epi = Long.parseLong(txt_serie_epi.getText());
        Serie s = new Serie();
            s.setTitulo(serie_titulo);
            s.setTemp(serie_tempo);
            s.setEpi(serie_epi);
        repositorio_Serie.save(s);
        //repositorioreto5.findById(dir_id);
        //repositorioreto5.delete(d);gf
        System.out.println("Serie agregada");
        jTextArea1.setText("La Serie " + serie_titulo + " ha sido agregada");
        borrarTextos();
        
        
        }
    }                                        

    private void Borrar() {                                       

        
        
            String Titulo = txt_serie_titulo.getText();
            List<Serie> s = repositorio_Serie.findByTitulo(Titulo);
            
            if(Titulo.equals("")){
                jTextArea1.setText("Información incompleta para borrar serie");
            }
            else{
            if(s.isEmpty()){
                jTextArea1.setText("No se encontro la serie");
                borrarTextos();
            }else{
                JFrame frame = new JFrame("frame");
                int respuesta = createUI(frame); 
                
                if(respuesta>0){
                repositorio_Serie.delete(s.get(0));
                //repositorioreto5.findById(dir_id);
                //repositorioreto5.delete(d);
                System.out.println("Serie borrada");
                jTextArea1.setText("La Serie " + Titulo + " ha sido borrada");
                borrarTextos();
            }
            }
        }
    }                                      

    private void Leer() {                                  
        String Titulo = txt_serie_titulo.getText();
        
        if(Titulo.equals("")){
            jTextArea1.setText("Debe ingresar el titulo de la serie");
        
        
        }
        else{
                List<Serie> s = repositorio_Serie.findByTitulo(Titulo);
            if(s.isEmpty()){
                System.out.println("No se encontro la serie");
                jTextArea1.setText("No se encontro la serie " + Titulo);
            }else{
                txt_serie_id.setText(s.get(0).getId().toString());
                txt_serie_titulo.setText(s.get(0).getTitulo());
                txt_serie_epi.setText(s.get(0).getEpi().toString());
                txt_serie_temp.setText(s.get(0).getTemp().toString());

                System.out.println("Lectura correcta");
                jTextArea1.setText("Lectura correcta");
            }
            }
    }
    
     private void visualTextos(boolean a, boolean b, boolean c, boolean d){
            txt_serie_id.setEnabled(a);
            txt_serie_titulo.setEnabled(b);
            txt_serie_epi.setEnabled(c);    
            txt_serie_temp.setEnabled(d);
            
    }
    
     private void borrarTextos(){
                    bandera = 0;
                    Opcion.setSelectedIndex(0);
                    txt_serie_epi.setText("");
                    txt_serie_id.setText("");
                    txt_serie_temp.setText("");
                    txt_serie_titulo.setText("");
     }
     
    private void OpcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpcionActionPerformed
        
        
        switch(Opcion.getSelectedIndex()) {
            case 1: // Crear
              visualTextos(false,true,true,true);
              bandera = 1;
              break;
            case 2: // Leer
              visualTextos(false,true,false,false);
              bandera = 2;
              break;
            case 3: // Actualizar
                visualTextos(false,true,true,true);  
                bandera = 3;
              break;
            case 4:
               visualTextos(false,true,false,false);
               bandera = 4;
              break;
            default:
                visualTextos(false,false,false,false);
                bandera = 0;
                //System.out.println("ninguna opción elegida");
                 //jTextArea1.setText("ninguna opción elegida");
              // code block
          }
    }//GEN-LAST:event_OpcionActionPerformed

    private void AceptarButton(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarButton
            
        switch(bandera) {
            case 1: // Crear
              Crear();
              bandera = 0;
              Opcion.setSelectedIndex(0);
              break;
            case 2: // Leer
              Leer();
              bandera = 0;
              Opcion.setSelectedIndex(0);  
              break;
            case 3: // Actualizar
              Actualizar();
              bandera = 0;
              Opcion.setSelectedIndex(0);
              break;
            case 4:
              Borrar();
              bandera = 0;
              Opcion.setSelectedIndex(0);
            break;
            default:

          }
    }//GEN-LAST:event_AceptarButton

    private void CancelarAceptarButton(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CancelarAceptarButton
        bandera = 0;
        Opcion.setSelectedIndex(0);
        jTextArea1.setText("");
        txt_serie_epi.setText("");
        txt_serie_id.setText("");
        txt_serie_temp.setText("");
        txt_serie_titulo.setText("");
    }//GEN-LAST:event_CancelarAceptarButton

    private void txt_serie_tempKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_serie_tempKeyPressed
        if (evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9' || evt.getKeyCode()  == evt.VK_BACK_SPACE) {
               txt_serie_temp.setEditable(true);
            } else {
               txt_serie_temp.setEditable(false);
            }
    }//GEN-LAST:event_txt_serie_tempKeyPressed

    private void txt_serie_idKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_serie_idKeyPressed
                if (evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9' || evt.getKeyCode()  == evt.VK_BACK_SPACE) {
               txt_serie_id.setEditable(true);
            } else {
               txt_serie_id.setEditable(false);
            }
    }//GEN-LAST:event_txt_serie_idKeyPressed

    private void txt_serie_epiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_serie_epiKeyPressed
            if (evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9' || evt.getKeyCode()  == evt.VK_BACK_SPACE) {
               txt_serie_epi.setEditable(true);
            } else {
               txt_serie_epi.setEditable(false);
            }
    }//GEN-LAST:event_txt_serie_epiKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Cancelar;
    private javax.swing.JComboBox<String> Opcion;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField txt_serie_epi;
    private javax.swing.JTextField txt_serie_id;
    private javax.swing.JTextField txt_serie_temp;
    private javax.swing.JTextField txt_serie_titulo;
    // End of variables declaration//GEN-END:variables
}
